from django.urls import path, include
from rest_framework import routers
from drf_quickstart import views


router = routers.DefaultRouter()
router.register(r"users", views.UserViewSet)
router.register(r"groups", views.GroupViewSet)


urlpatterns = [
    path("", include(router.urls)),
    path("api_auth/", include("rest_framework.urls", namespace="rest_framework"))
]
